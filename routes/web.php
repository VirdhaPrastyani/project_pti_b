<?php

use Illuminate\Support\Facades\Route;

Route::get('/index', [App\Http\Controllers\IndexController::class,'index']); 
Route::get('/about', [App\Http\Controllers\AboutController::class,'about']);
